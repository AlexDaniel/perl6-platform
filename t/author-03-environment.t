use v6.c;
use lib 't/lib';
use Test;
use Template;

plan 10;

constant AUTHOR = ?%*ENV<AUTHOR_TESTING>;

if not AUTHOR {
     skip-rest "Skipping author test";
     exit;
}

my $tmpdir = '.tmp/test-platform-05-environment'.IO.absolute;
run <rm -rf>, $tmpdir if $tmpdir.IO.e;
mkdir $tmpdir;

ok $tmpdir.IO.e, "got $tmpdir";

sub create-project(Str $animal) {
    my $project-dir = $tmpdir ~ "/project-" ~ $animal.lc;
    my %project =
        title => "Project " ~ parse-names($animal),
        name => "project-" ~ $animal.lc
    ;
    mkdir "$project-dir/docker";
    spurt "$project-dir/docker/Dockerfile", docker-dockerfile(%project);
    my $project-yml = q:heredoc/END/;
        command: nginx -g 'daemon off;'
        volumes:
            - html:/usr/share/nginx/html:ro
        END
    spurt "$project-dir/docker/project.yml", $project-yml;
    mkdir "$project-dir/html";
    spurt "$project-dir/html/index.html", html-welcome(%project);
    $project-dir;
}

sub create-ssh-project(Str $animal) {
    my $project-dir = create-project($animal);
    my $new-dockerfile = q:heredoc/END/;
        FROM nginx:latest
        RUN apt-get update
        RUN apt-get -y install openssh-server
        END
    spurt "$project-dir/docker/Dockerfile", $new-dockerfile;
    my $new-projectyml = q:heredoc/END/;
        command: /bin/bash -c "/etc/init.d/ssh start && nginx -g 'daemon off;'"
        volumes:
            - html:/usr/share/nginx/html:ro
        END
    spurt "$project-dir/docker/project.yml", $new-projectyml;
    $project-dir;
}

sub project-is-running(Str $project-name) {
    my $inspect = run <docker inspect -f \"{{.State.Running}}\">, $project-name, :out, :err;
    if (! $inspect.exitcode) {
        my $out = $inspect.out.slurp-rest.chomp.subst('"', '', :g).subst('\\', '', :g);
        return $out.trim eq 'true';
    }
    return False;
}

sub project-is-stopped(Str $project-name) {
    my $inspect = run <docker inspect -f \"{{.State.ExitCode}}\">, $project-name, :out, :err;
    if (! $inspect.exitcode) {
        my $out = $inspect.out.slurp-rest.chomp.subst('"', '', :g).subst('\\', '', :g);
        return $out.trim == 137; # Stopped containers have exit code 137
    }
    return False;
}

my $domain-amazon = 'amazon';
my $domain-sahara = 'sahara';

#
# General setup
#
subtest 'platform create', {
    plan 2;
    my $proc = run <bin/platform>, <create>, "--data-path=$tmpdir/.platform", :out;
    $proc.out.slurp-rest;

    ok project-is-running("platform-dns"), 'service dns is up';
    ok project-is-running("platform-proxy"), 'service proxy is up';
}

subtest "platform --domain=$domain-sahara ssh --keygen", {
    plan 3;
    run <bin/platform>, "--domain=$domain-sahara", "--data-path=$tmpdir/.platform", <ssh --keygen>, :out;
    ok "$tmpdir/.platform/$domain-sahara/ssh".IO.e, "<data>/$domain-sahara/ssh exists";
    ok "$tmpdir/.platform/$domain-sahara/ssh/$_".IO.e, "<data>/$domain-sahara/ssh/$_ exists" for <id_rsa id_rsa.pub>;
}

subtest "platform --domain=$domain-amazon ssh --keygen", {
    plan 3;
    run <bin/platform>, "--domain=$domain-amazon", "--data-path=$tmpdir/.platform", <ssh --keygen>, :out;
    ok "$tmpdir/.platform/$domain-amazon/ssh".IO.e, "<data>/$domain-amazon/ssh exists";
    ok "$tmpdir/.platform/$domain-amazon/ssh/$_".IO.e, "<data>/$domain-amazon/ssh/$_ exists" for <id_rsa id_rsa.pub>;
}

#
# Start 2 projects under *.sahara domain with single command and project's 
# default settings
#
subtest "platform .. sahara.yml run", {
    plan 4;

    create-project('scorpion');
    create-project('ant');

    my $environment-yml = q:heredoc/END/;
        project-scorpion: true
        project-ant: true
        END

    spurt "$tmpdir/sahara.yml", $environment-yml;

    my $proc = run <bin/platform>, <run>, "$tmpdir/sahara.yml", "--domain=sahara", "--data-path=$tmpdir/.platform", :out;
    $proc.out.slurp-rest;
    ok project-is-running("project-scorpion"), "project-scorpion run";
    ok project-is-running("project-ant"), "project-ant run";

    sleep 1.5;

    for <scorpion ant> -> $project {
        $proc = run <docker exec -it platform-proxy getent hosts>, "project-{$project}.sahara", :out;
        my $out = $proc.out.slurp-rest;
        my $found = $out.Str.trim ~~ / ^ $<ip-address> = [ \d+\.\d+\.\d+\.\d+ ] /;
        ok $found, "got project-$project.sahara ip-address " ~ ($found ?? $/.hash<ip-address> !! '');
    }
}

#
# Start 2 projects under *.amazon domain with single command and override
# project's default settings
#
subtest "platform .. amazon.yml run", {
    plan 6;

    create-ssh-project('octopus');
    create-ssh-project('blowfish');

    my $environment-yml = q:heredoc/END/;
        project-octopus:
            users:
              octonaut:
                system: true
                home: /var/lib/octonaut
                shell: /bin/bash
            dirs:
              /var/lib/octonaut/.ssh:
                owner: octonaut
                group: nogroup
                mode: 0700
            files:
              /var/lib/octonaut/.ssh/id_rsa:
                content: ssh/id_rsa
                owner: octonaut
                group: nogroup
                mode: 0600
        project-blowfish:
            users:
              kwazii:
                shell: /bin/bash
            dirs:
              /home/kwazii/.ssh:
                owner: kwazii
                group: kwazii
                mode: 0700
            files:
              /home/kwazii/.ssh/authorized_keys:
                content: ssh/id_rsa.pub
                owner: kwazii
                group: kwazii
                mode: 0600
        END

    spurt "$tmpdir/amazon.yml", $environment-yml;
    
    my $proc = run <bin/platform>, <run>, "$tmpdir/amazon.yml", "--domain=amazon", "--data-path=$tmpdir/.platform", :out;
    my $out = $proc.out.slurp-rest;
    ok project-is-running("project-octopus"), "project-octopus run";
    ok project-is-running("project-blowfish"), "project-blowfish run";

    sleep 1.5;

    my %addr;
    for <octopus blowfish> -> $project {
        $proc = run <docker exec -it platform-proxy getent hosts>, "project-{$project}.amazon", :out;
        $out = $proc.out.slurp-rest;
        my $found = $out.Str.trim ~~ / ^ $<ip-address> = [ \d+\.\d+\.\d+\.\d+ ] /;
        %addr{$project} = $/.hash<ip-address>;
        ok $found, "got project-$project.amazon ip-address " ~ ($found ?? $/.hash<ip-address> !! '');    
    }

    $proc = run <docker exec -it project-octopus su octonaut --command>, "ssh -o \"StrictHostKeyChecking no\" kwazii\@{%addr<blowfish>} ls /", :out;
    $out = $proc.out.slurp-rest;
    is $out.lines.elems, 21, 'got proper response from ssh connection';

    $proc = run <docker exec -it project-octopus getent hosts project-blowfish.amazon>, :out;
    $out = $proc.out.slurp-rest;
    is $out.trim, %addr<blowfish> ~ '      project-blowfish.amazon', 'got project-blowfish.amazon ip inside container';
}

subtest "platform .. sahara.yml stop|rm", {
    plan 4;

    my $proc = run <bin/platform>, <stop>, "$tmpdir/sahara.yml", "--data-path=$tmpdir/.platform", :out;
    my $out = $proc.out.slurp-rest;
    ok project-is-stopped("project-scorpion"), 'project-scorpion stop';
    ok project-is-stopped("project-ant"), 'project-ant stop';

    $proc = run <bin/platform>, <rm>, "$tmpdir/sahara.yml", "--data-path=$tmpdir/.platform", :out;
    $out = $proc.out.slurp-rest;
    ok ! project-is-running("project-scorpion"), 'project-scorpion rm';
    ok ! project-is-running("project-ant"), 'project-ant rm';
}

subtest "platform .. amazon.yml stop|rm", {
    plan 4;

    my $proc = run <bin/platform>, <stop>, "$tmpdir/amazon.yml", "--data-path=$tmpdir/.platform", :out;
    my $out = $proc.out.slurp-rest;
    ok project-is-stopped("project-octopus"), 'project-octopus stop';
    ok project-is-stopped("project-blowfish"), 'project-blowfish stop';

    $proc = run <bin/platform>, <rm>, "$tmpdir/amazon.yml", "--data-path=$tmpdir/.platform", :out;
    $out = $proc.out.slurp-rest;
    ok ! project-is-running("project-octopus"), 'project-octopus rm';
    ok ! project-is-running("project-blowfish"), 'project-blowfish rm';
}

#
# Start 3 projects from a single yml and make sure the order they are started is correct
#
subtest "platform .. atacama.yml run", {
    plan 5;

    create-project('flamingo');
    create-project('vicugna');
    create-project('armadillo');

    my $environment-yml = q:heredoc/END/;
        build-order:
           - project-vicugna
           - project-flamingo
        project-vicugna:
           users:
              test:
                 shell: /bin/sh
           files:
              /home/test/touch-this-first:
                 content: 'test'
                 owner: test
                 group: nogroup
                 mode: 0600
        project-flamingo:
           users:
              test:
                 shell: /bin/sh
           files:
              /home/test/touch-this-second:
                 content: 'test'
                 owner: test
                 group: nogroup
                 mode: 0600
        project-armadillo:
           users:
              test:
                 shell: /bin/sh
           files:
              /home/test/touch-this-third:
                 content: 'test'
                 owner: test
                 group: nogroup
                 mode: 0600
        END

    spurt "$tmpdir/atacama.yml", $environment-yml;

    my $proc = run <bin/platform>, <run>, "$tmpdir/atacama.yml", "--data-path=$tmpdir/.platform", :out;
    $proc.out.slurp-rest;
    ok project-is-running("project-vicugna"), "project-vicugna run";
    ok project-is-running("project-flamingo"), "project-flamingo run";
    ok project-is-running("project-armadillo"), "project-armadillo run";

    sleep 1.5;

    my $first-stat-proc = run <docker exec -it project-vicugna /bin/sh -c>, "stat -c \%Y /home/test/touch-this-first", :out;
    my $first-created = $first-stat-proc.out.slurp-rest;
    my $second-stat-proc = run <docker exec -it project-flamingo /bin/sh -c>, "stat -c \%Y /home/test/touch-this-second", :out;
    my $second-created = $second-stat-proc.out.slurp-rest;
    my $third-stat-proc = run <docker exec -it project-armadillo /bin/sh -c>, "stat -c \%Y /home/test/touch-this-third", :out;
    my $third-created = $third-stat-proc.out.slurp-rest;

    ok $first-created < $second-created, "project-vicugna file was created first";
    ok $second-created < $third-created, "project-flamingo file was created second";
}

subtest "platform .. atacama.yml rm", {
    plan 3;
    my $proc = run <bin/platform>, <remove>, "$tmpdir/atacama.yml", "--data-path=$tmpdir/.platform", :out;
    $proc.out.slurp-rest;
    ok ! project-is-running("project-vicugna"), "project-vicugna rm";
    ok ! project-is-running("project-flamingo"), "project-flamingo rm";
    ok ! project-is-running("project-armadillo"), "project-armadillo rm";
}

run <bin/platform destroy>, :out;
