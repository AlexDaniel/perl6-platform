use v6;
use YAMLish;
use App::Platform::Project;
use App::Platform::Git;
use App::Platform::Output;
use Terminal::ANSIColor;

class App::Platform::Environment {

    has Str $.network = 'acme';
    has Str $.domain = 'localhost';
    has Str $.data-path is rw;
    has Str $.environment;
    has Str @.reserved-keys = [ 'type', 'name', 'desc', 'build-order' ];
    has Bool $.skip-dotfiles = False;

    has App::Platform::Project @.projects;
    has App::Platform::Container @.containers;

    submethod TWEAK {
        my %config = load-yaml $!environment.IO.slurp;

        my @project-keys = %config.keys.grep( { not $_ (elem) @!reserved-keys } );

        if %config<build-order> :exists {
            if %config<build-order> !~~ Array {
                die "Build-order must be an Array, got {%config<build-order>.WHAT.^name}";
            }
            my @build-order = @(%config<build-order>);
            for @build-order -> $project {
                if ! @project-keys.contains($project) {
                    die "Build-order had a project ($project) that is not listed in configuration";
                }
            }
            @project-keys = flat @build-order, @project-keys.grep( { not $_ (elem) @build-order } );
        }

        for @project-keys -> $project {
            next if @!reserved-keys.contains($project);

            my $data = %config<<$project>>;

            # Git support. Try to fetch repository if it does not exist
            put '🐱' ~ App::Platform::Output.after-prefix ~ color('yellow'), ~ 'Repositories' ~ color('reset');

            App::Platform::Git.new(
                data    => $data<git>,
                target  => $!environment.IO.parent ~ '/' ~ $project
                ).clone if $data<git>;

            my $project-path = $project ~~ / ^ \/ / ?? $project !! "{self.environment.IO.dirname}/{$project}".IO.absolute;
            if $data ~~ Bool and $data {
                @!projects.push: App::Platform::Project.new(
                    :domain($!domain),
                    :data-path($!data-path),
                    :project($project-path),
                    :skip-dotfiles($!skip-dotfiles)
                    );
            } elsif $data ~~ Hash {
                @!projects.push: App::Platform::Project.new(
                    :domain($!domain),
                    :data-path($!data-path),
                    :project($project-path),
                    :override($data.Hash),
                    :skip-dotfiles($!skip-dotfiles)
                    );
            }
        }
    }

    method run { @.projects.map: { @.containers.push(.run) }; self }

    method start { @.projects.map: { @.containers.push(.start) }; self }

    method stop { @.projects.map: { @.containers.push(.stop) }; self }

    method rm { @.projects.map: { @.containers.push(.rm) }; self }

    method as-string { @.containers.map({.as-string}).join("\n") }

}
